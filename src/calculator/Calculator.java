
package calculator;
public abstract class Calculator {
   public double operand1;
   public double operand2;

    public Calculator(double operand1, double operand2) {
        this.operand1 = operand1;
        this.operand2 = operand2;
    }
    protected abstract double calculate();
 }
